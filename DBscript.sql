/****** Object:  StoredProcedure [dbo].[UserLogin]    Script Date: 21-06-2021 09:23:42 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserLogin]
GO
/****** Object:  StoredProcedure [dbo].[UserInfoDetails]    Script Date: 21-06-2021 09:23:42 ******/
DROP PROCEDURE IF EXISTS [dbo].[UserInfoDetails]
GO
/****** Object:  StoredProcedure [dbo].[InsertUpdateUserInfo]    Script Date: 21-06-2021 09:23:42 ******/
DROP PROCEDURE IF EXISTS [dbo].[InsertUpdateUserInfo]
GO
/****** Object:  StoredProcedure [dbo].[DropUserRole]    Script Date: 21-06-2021 09:23:42 ******/
DROP PROCEDURE IF EXISTS [dbo].[DropUserRole]
GO
/****** Object:  StoredProcedure [dbo].[DeleteUserInfo]    Script Date: 21-06-2021 09:23:42 ******/
DROP PROCEDURE IF EXISTS [dbo].[DeleteUserInfo]
GO
/****** Object:  Table [dbo].[USER_ROLE]    Script Date: 21-06-2021 09:23:42 ******/
DROP TABLE IF EXISTS [dbo].[USER_ROLE]
GO
/****** Object:  Table [dbo].[USER_LOGIN]    Script Date: 21-06-2021 09:23:42 ******/
DROP TABLE IF EXISTS [dbo].[USER_LOGIN]
GO
/****** Object:  Table [dbo].[USER_LOGIN]    Script Date: 21-06-2021 09:23:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[USER_LOGIN](
	[USER_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[USER_NAME] [varchar](50) NOT NULL,
	[PASSWORD] [nvarchar](50) NOT NULL,
	[ROLE_ID] [bigint] NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[CustomerID] [bigint] NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[CREATED_BY] [nvarchar](500) NOT NULL,
	[CREATED_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_USER_LOGIN] PRIMARY KEY CLUSTERED 
(
	[USER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[USER_ROLE]    Script Date: 21-06-2021 09:23:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[USER_ROLE](
	[ROLE_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ROLE_NAME] [varchar](50) NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[CREATED_BY] [nvarchar](500) NOT NULL,
	[CREATED_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_USER_ROLE] PRIMARY KEY CLUSTERED 
(
	[ROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[USER_LOGIN] ON 

INSERT [dbo].[USER_LOGIN] ([USER_ID], [USER_NAME], [PASSWORD], [ROLE_ID], [Email], [CustomerID], [IS_ACTIVE], [CREATED_BY], [CREATED_DATE]) VALUES (5, N'Vivek2989', N'mh71OvVOnUZ0hoU0blQOsUkEHVqrhvcSRfRWP4E4SWA=', 2, N'vivek2989@xyz.com', 1, 1, N'ADMIN', CAST(N'2021-06-19T04:00:21.617' AS DateTime))
INSERT [dbo].[USER_LOGIN] ([USER_ID], [USER_NAME], [PASSWORD], [ROLE_ID], [Email], [CustomerID], [IS_ACTIVE], [CREATED_BY], [CREATED_DATE]) VALUES (6, N'sankar2110', N'CSBBffUK3m3uyQc8HdPGMJHKFOKXtpaq523nM7iK9n0=', 1, N'sankar14@gmail.com', 1, 1, N'ADMIN', CAST(N'2021-06-21T09:17:04.377' AS DateTime))
SET IDENTITY_INSERT [dbo].[USER_LOGIN] OFF
GO
SET IDENTITY_INSERT [dbo].[USER_ROLE] ON 

INSERT [dbo].[USER_ROLE] ([ROLE_ID], [ROLE_NAME], [IS_ACTIVE], [CREATED_BY], [CREATED_DATE]) VALUES (1, N'Admin', 1, N'1', CAST(N'2021-06-02T00:00:00.000' AS DateTime))
INSERT [dbo].[USER_ROLE] ([ROLE_ID], [ROLE_NAME], [IS_ACTIVE], [CREATED_BY], [CREATED_DATE]) VALUES (2, N'Manager', 1, N'1', CAST(N'2021-06-04T00:00:00.000' AS DateTime))
INSERT [dbo].[USER_ROLE] ([ROLE_ID], [ROLE_NAME], [IS_ACTIVE], [CREATED_BY], [CREATED_DATE]) VALUES (4, N'Cashier', 1, N'1', CAST(N'2021-06-02T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[USER_ROLE] OFF
GO
/****** Object:  StoredProcedure [dbo].[DeleteUserInfo]    Script Date: 21-06-2021 09:23:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteUserInfo]
@USERID BIGINT
AS
BEGIN
	
	UPDATE USER_LOGIN SET [IS_ACTIVE] = 0 WHERE [USER_ID] = @USERID
	END

GO
/****** Object:  StoredProcedure [dbo].[DropUserRole]    Script Date: 21-06-2021 09:23:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DropUserRole]
AS
BEGIN
	
	SELECT [ROLE_ID] USERROLE,[ROLE_NAME] Name FROM [dbo].[USER_ROLE]
	END




GO
/****** Object:  StoredProcedure [dbo].[InsertUpdateUserInfo]    Script Date: 21-06-2021 09:23:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertUpdateUserInfo]
@USERID BIGINT,
@USERNAME varchar(50),
@UPASSWORD nvarchar(50),
@USERROLE BIGINT,
@Email nvarchar(250),
@CustomerID  BIGINT
AS
BEGIN
	
	IF(@USERID = 0)
	INSERT INTO USER_LOGIN
	(
	[USER_NAME],
	[PASSWORD],
	[ROLE_ID],
	[Email],
	[CustomerID],
	[IS_ACTIVE],
	[CREATED_BY],
	[CREATED_DATE]
	)
	VALUES
	(
	@USERNAME,
	@UPASSWORD,
	@USERROLE,
	@Email,
	@CustomerID,
	1,
	'ADMIN',
	GETDATE()
	)
	ELSE
	UPDATE USER_LOGIN SET [USER_NAME] = @USERNAME,[PASSWORD] = @UPASSWORD,[ROLE_ID] = @USERROLE,
	[Email] = @Email,[CustomerID] = [CustomerID] WHERE [USER_ID] = @USERID
	END

GO
/****** Object:  StoredProcedure [dbo].[UserInfoDetails]    Script Date: 21-06-2021 09:23:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserInfoDetails]
AS
BEGIN
	
	SELECT [USER_ID] AS USERID,[USER_NAME] USERNAME,[Email] Email,
	[PASSWORD] UPASSWORD,[ROLE_ID] USERROLE,[USER_NAME] Name FROM USER_LOGIN
	END

GO
/****** Object:  StoredProcedure [dbo].[UserLogin]    Script Date: 21-06-2021 09:23:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserLogin]
@USERNAME NVARCHAR(50),
@UPASSWORD NVARCHAR(50)
AS
BEGIN
	SELECT [USER_ID] USERID,[USER_NAME] USERNAME,[PASSWORD] UPASSWORD,
	[ROLE_ID] AS USERROLE,[IS_ACTIVE] ISACTIVE FROM [dbo].[USER_LOGIN] WHERE [USER_NAME] = @USERNAME AND [PASSWORD] = @UPASSWORD
END
GO
